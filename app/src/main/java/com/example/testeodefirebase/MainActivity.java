package com.example.testeodefirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;

public class MainActivity extends AppCompatActivity {
    Button track_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        track_button = findViewById(R.id.track_button);
    }

    public void click(View view){
        //Con este intent se manejan las ventanas entre menu y fragment del mapa cuando inicia la actividad
        Intent cambioDeventana = null;
        switch (view.getId()){
            case R.id.track_button:
                cambioDeventana = new Intent(MainActivity.this, MapsActivity.class);
                break;
        }
        startActivity(cambioDeventana);
    }


}
